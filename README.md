# GuixSD_System.scm

the system.scm configure of mine

```scheme
(use-modules (gnu))

;; try guix system vm first!!! and use guix system reconfigure or guix system init /mnt to install it
(use-service-modules desktop networking ssh xorg)
(use-package-modules base linux
		     text-editors emacs
		     tmux ssh
		     wm xfce shells gnome gnome-xyz gtk xdisorg compton lxde video
		     irc mail messaging
		     gnupg tls
		     version-control  bittorrent
                     containers package-management
		     disk curl admin
		     sqlite compression
		     fcitx5 mpd
                     image-viewers
                     fonts certs
		     maths
		     guile guile-xyz)

(operating-system
 (host-name "coincoinv")
 (timezone "Asia/Shanghai")
 ;; zh_CN.utf8 won't work good with gdm, use the environment to configure the LANG
 (locale "en_US.utf8")

 (bootloader (bootloader-configuration
              ;; don't make mistake of the EFI mount!!!
              (bootloader grub-efi-bootloader)
              (targets '("/boot"))))
 ;; dvorak is good
 (keyboard-layout (keyboard-layout "us" "dvorak"))
 (file-systems (append (list
                        (file-system
                         ;; actually you can use the /dev/sda2 instead of (uuid "balabalabal"), nevermind
                         (device (uuid "3e957156-08da-497d-b39a-6e188ff0a48e"))
                         (mount-point "/")
                         ;; subvol is good, you can add some compress here
                         (options "subvol=@guix-system")
                         (type "btrfs"))
                        (file-system
                         (device "/dev/sda3")
                         (mount-point "/home")
                         (type "btrfs"))
                        (file-system
                         (device "/dev/sda1")
                         (mount-point "/boot")
                         (type "vfat"))
                        )
               %base-file-systems))
 (users (cons (user-account
               (name "coincoinv")
               (comment "CoinCoinV")
               (uid 1000)
               (password (crypt "give_my_money" "no_salt"))
               (group "users")
               (supplementary-groups '("wheel" "audio" "video" "netdev")))
              %base-user-accounts))
 ;; system package configure
 ;; awesome won't use the xfce4-terminal as the default terminal, need a wm-config.tar.gz
 ;; what kind of job.xsession do? export LANG=zh_CN.utf8; awesome won't work good as i expect
 ;; use guix shell to use the openjdk/nimble/clang or user-namespace installing
 (packages (append (list xfce4-terminal tmux emacs mg git
                         awesome dino lxappearance ranger p7zip scrot picom gnumeric
			 transmission
                         fcitx5 (list fcitx5-gtk "gtk3") (list fcitx5-gtk "gtk2") fcitx5-chinese-addons fcitx5-configtool
			 mpd ncmpcpp
			 guile-lib guile-readline guile-gcrypt guile-pfds
			 pipewire
			 gnuplot
			 libxtst
                         hicolor-icon-theme numix-gtk-theme
                         weechat rofi mutt qiv netcat
                         gnutls gnupg sqlite flatpak nss-certs curl
                         ;; font is important to show chinese
                         font-openmoji font-wqy-microhei)
	   ;; base packages
                   %base-packages))
 (services (append (list (service openssh-service-type
                                 (openssh-configuration
                                  (openssh openssh-sans-x)
                                  (port-number 22)))
                         (set-xorg-configuration
                          (xorg-configuration
                           (keyboard-layout keyboard-layout)))
                         )
		   ;; modify the substitute
                   (modify-services %desktop-services
                   (guix-service-type config =>
                                      (guix-configuration
                                       (inherit config)
                                       (substitute-urls
                                        (append (list "https://mirror.sjtu.edu.cn/guix")
                                                %default-substitute-urls))
                                       (tmpdir "/gnu/build"))))))
 )
```
