(define-module (coincoinv packages parser)
  #:use-module (gnu packages)
  #:use-module (guix packages)
  #:use-module (guix build-system cmake)
  #:use-module (guix git-download)
  #:use-module ((guix licenses) #:prefix license:)
  )
(define-public libmyhtml
  (package
   (name "libmyhtml")
   (version "4.0.5")
   (source
    (origin
     (method git-fetch)
     (uri (git-reference
           (url "https://github.com/lexborisov/myhtml")
           (commit (string-append "v" version))
           )
          )
     (sha256
      (base32 "0nan7bp5rvnh3v2m67pjnbwc6m91rfb4f4ml2pr8f14p939xgf05"))
     ))
   (build-system cmake-build-system)
   (arguments `(#:phases
                (modify-phases %standard-phases
                               (delete 'check))
                ))
   (synopsis "c/c++ html parser library")
   (description "MyHTML is a fast HTML Parser using Threads implemented as a pure C99 library with no outside dependencies.")
   (home-page "https://github.com/lexborisov/myhtml")
   (license license:lgpl3+))
   )
  
libmyhtml
