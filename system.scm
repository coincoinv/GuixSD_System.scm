(use-modules (gnu))

;; try guix system vm first!!! and use guix system reconfigure or guix system init /mnt to install it
(use-service-modules desktop networking ssh xorg)
(use-package-modules base linux disk
		     text-editors emacs tmux ssh
		     wm
         xfce
         shells
         gnome
         gnome-xyz
         gtk
         xdisorg
         xorg
         compton
         lxde
         freedesktop
         disk
         admin
         networking
         curl
         tls
         vpn
         video
         w3m
		     irc
         mail
         messaging
		     gnupg
		     version-control
         bittorrent
         containers
         package-management
		     sqlite
         compression
		     fcitx
         mpd
         pulseaudio
         image-viewers
         fonts
         certs
         search
		     maths
		     guile
         guile-xyz)

(operating-system
 (host-name "coincoinv")
 (timezone "Asia/Shanghai")
 ;; zh_CN.utf8 won't work good with gdm, use the environment to configure the LANG
 (locale "en_US.utf8")

 (bootloader (bootloader-configuration
              ;; don't make mistake of the EFI mount!!!
              (bootloader grub-efi-bootloader)
              (targets '("/boot"))))
 ;; dvorak is good
 (keyboard-layout (keyboard-layout "us" "dvorak"))
 (file-systems (append (list
                        (file-system
                         ;; actually you can use the /dev/sda2 instead of (uuid "balabalabal"), nevermind
                         (device (uuid "3e957156-08da-497d-b39a-6e188ff0a48e"))
                         (mount-point "/")
                         ;; subvol is good, you can add some compress here
                         (options "subvol=@guix-system")
                         (type "btrfs"))
                        (file-system
                         (device "/dev/sda3")
                         (mount-point "/home")
                         (type "btrfs"))
                        (file-system
                         (device "/dev/sda1")
                         (mount-point "/boot")
                         (type "vfat"))
                        )
               %base-file-systems))
 (users (cons (user-account
               (name "coincoinv")
               (comment "CoinCoinV")
               (uid 1000)
               (password (crypt "give_my_money" "no_salt"))
               (group "users")
               (supplementary-groups '("wheel" "audio" "video" "netdev")))
              %base-user-accounts))
 (hosts-file
  (plain-file "hosts"
              (string-append (local-host-aliases host-name)
                             "10.5.0.1\topenwrt\n"
                             "10.5.0.200\trouter\n"
                             )
              )
  )
 ;; system package configure
 ;; awesome won't use the xfce4-terminal as the default terminal, need a wm-config.tar.gz
 ;; what kind of job.xsession do? export LANG=zh_CN.utf8; awesome won't work good as i expect
 ;; use guix shell to use the openjdk/nimble/clang or user-namespace installing
 (packages (append (list
                    xfce4-terminal
                    tmux
                    emacs
                    mg
                    git
                    awesome
                    dino
                    lxappearance
                    ranger
                    p7zip
                    scrot
                    picom
                    gnumeric
			              transmission
                    blueman
                    fcitx (list fcitx "gtk2") (list fcitx "gtk3") fcitx-configtool
			              mpd
                    ncmpcpp
			              guile-lib
                    guile-readline
                    guile-gcrypt
                    guile-pfds
			              pipewire pamixer
                    wireguard-tools
			              gnuplot
			              libxtst
                    ugrep
                    dosfstools
                    btrfs-progs
                    hicolor-icon-theme
                    numix-gtk-theme
                    catgirl
                    rofi
                    mutt
                    qiv
                    netcat
                    xsel
                    mpv
                    w3m
                    xdg-utils
                    ;; i don't add the firewall rule here, because  i have no such need, i got it at my openwrt
                    gnutls nftables
                    gnupg
                    sqlite
                    flatpak
                    nss-certs
                    curl
                    ;; font is important to show chinese
                    font-openmoji font-wqy-microhei)
	                 ;; base packages
                   %base-packages))
 (services (append (list (service openssh-service-type
                                  (openssh-configuration
                                   (openssh openssh-sans-x)
                                   (port-number 22)))
                         (set-xorg-configuration
                          (xorg-configuration
                           (keyboard-layout keyboard-layout)))
                         )
		               ;; modify the substitute
                   (modify-services %desktop-services
                                    (guix-service-type config =>
                                                       (guix-configuration
                                                        (inherit config)
                                                        (substitute-urls
                                                         (append (list "https://mirror.sjtu.edu.cn/guix")
                                                                 %default-substitute-urls))
                                                        ;; add a build dir for some big package build like browser
                                                        (tmpdir "/gnu/build"))))))
 (name-service-switch %mdns-host-lookup-nss)
 )
